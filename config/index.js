const config = {
    output: {
      publicPath: '/html'
    },
    dev: {
        port: 9009
    }
}

module.exports = config